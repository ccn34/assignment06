/*Main.js to accompany assignment06 */
/*code to alter the date to current*/ 

window.onload = function date_and_season() {
    "use strict";
    // variables
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        // Getting the date
        full_date = new Date(),
        this_month_text = monthNames[full_date.getMonth()],
        // ^^ the name of the month
        today = full_date.getDate(),
        this_year = full_date.getFullYear(),
       
        new_span = document.createElement('span'),
        // text string 
        new_text =  document.createTextNode(today +' ' + this_month_text + ' ' + this_year),
        position = document.getElementsByTagName('footer')[0];
    // append date to footer
    new_span.appendChild(new_text);
    position.appendChild(new_span);
};

//--------------------------------------------------//
//setting up next and previous buttons
var index = 0;
var pix = ["spring.jpg", "summer.jpg", "fall.jpg", "winter.jpg"];
var main_pic=document.getElementById('spring.jpg')

function decr_index() {
   
    "use strict";
    
    
    var main_pic = document.getElementById('spring');
    
    
    // loop to activate "previous" button
    if(index == 0){
        index = pix.length - 1
    }
    else {
        index-- //index = index - 1
    }
    
    console.log("index")
    
   
    main_pic.src = 'images/' + pix[index]; 
}

function incr_index() {
   
    "use strict";
    
    
    var main_pic = document.getElementById('spring');
    //loop to activate "next" button
    if(index == 3){
        index = pix.length - 4
    }
    else {
        index++ //index = index + 1
    }
    
    
    main_pic.src = 'images/' + pix[index];
}
